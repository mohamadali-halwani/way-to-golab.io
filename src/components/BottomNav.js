import React from 'react';
import {Link} from 'react-router-dom';
import {useAsync} from 'react-async';
import {concat} from 'lodash';

import content from '../content';
import categorizedPages from '../categorizedPages';

export default function BottomNav({page, locale}) {
    const {data: navigation} = useAsync({
        promiseFn: content,
        watch: `./${locale}/navigation.yaml`,
        path: `./${locale}/navigation.yaml`,
    });

    if (!navigation) {
        return null;
    }

    const pageOrder = concat(
        ...categorizedPages.map(cat => cat.pages)
    );
    const pageIndex = pageOrder.indexOf(page);

    return <div>
        {pageIndex > 0 &&
            <Link to={`/${locale}/${pageOrder[pageIndex - 1]}`} className="prev-btn" onClick={scrollToTop}>
                <i className="fas fa-chevron-left"></i> {navigation[`previous`]}
            </Link>
        }

        {pageIndex + 1 < pageOrder.length &&
            <Link to={`/${locale}/${pageOrder[pageIndex + 1]}`} className="next-btn" onClick={scrollToTop}>
                {navigation[`next`]} <i className="fas fa-chevron-right"></i>
            </Link>
        }
    </div>;
}

function scrollToTop() {
    window.scrollTo(0, 0);
}
