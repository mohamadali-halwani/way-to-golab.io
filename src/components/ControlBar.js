import React from 'react';
import next from '../next.svg';
import prev from '../prev.svg';
import reset from '../reset.svg';
import success from '../success.svg';
import fail from '../fail.svg';

export default function ControlBar({onReset, onNext, onPrev, showSuccess, showFail}) {
    return <div className='control-bar'>
        <div className='control-bar-card'>
            <img className='control-button' src={reset} alt='reset' onClick={onReset}/>
            {onPrev && <img className='control-button' src={prev} alt='prev' onClick={onPrev}/>}
            {onNext && <img className='control-button' src={next} alt='next' onClick={onNext}/>}

            {showFail && <img className='control-status' src={fail} alt='fail'/>}
            {showSuccess && <img className='control-status' src={success} alt='success'/>}
        </div>
    </div>;
}
