Now that you have seen diagonally connected stones get cut, we can look at how
to defend against cuts.

Board::defend-1

Here, white can cut the black stones.  However, black can first play a move to
defend against the cut, playing what is called a **connection** move.

---

Board::defend-2

On this board,  black has made a **direct connection**.  There is no
possibility for white to disconnect black.

---

Here, we have an **indirect connection**, sometimes called a **hanging
connection**.

Board::defend-3

White can legally play to cut black, but black can immediately capture the
stone so black is, in essence, connected.

An indirect connection is sometimes better than connecting directly.  If you
can determine when it makes more sense to connect indirectly, you are no longer
a beginner.

---

Board::defend-4

Above, black has two cutting points.  It may stand to reason that black can
only protect one.

However, the marked move serves as an indirect connection for both cut points.

Because of the shape, this connection is sometimes called a **trumpet
connection**.
