This is a basic tutorial for the game of [Go][go-game] adapted from Hiroki
Mori's [Interactive Way to Go][iwtg].

### How to help

* Know another language?  Available to help translate?  Please reach out by
  opening an [issue on GitLab][gitlab].  There's no need to know anything about
  programming!
* Find a bug?  Open an [issue][gitlab]!

### Translators

Special thanks to the following translators from the community!

::Translators::

### Acknowledgments

* Hiroki Mori wrote both the content and code for the original [Interactive Way
  to Go][iwtg] in 1997, reaching thousands of new players.
* With Hiroki's permission, [duckpunch][] rewrote the code using modern methods
  in 2019 and keeping much of the same original content.

[duckpunch]: https://duckpunch.org
[gitlab]: https://gitlab.com/way-to-go/way-to-go.gitlab.io
[go-game]: https://en.wikipedia.org/wiki/Go_(game)
[iwtg]: https://web.archive.org/web/20210831172631/http://playgo.to/iwtg/en/
