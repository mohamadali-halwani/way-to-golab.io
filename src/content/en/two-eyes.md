Groups of stones can be captured once they are completely surrounded.  The
black stones below are in such a predicament - nowhere to escape and awaiting
capture by white.

Board::trouble

---

Black can play freely once again.  White will not answer, but be on the lookout
for illegal moves.

Board::heaven

## Two eyes

Consider the case below.

Board::life

While the black stones are surrounded, white cannot capture them.  There are
still two spaces required, both of which are illegal moves.

We call separate internal spaces **eyes**.  Once a group makes 2 **eyes**, it
cannot be captured.  We can call the group **unconditionally alive**.

Try to capture the white stones on the board once again.

Board::unkillable

Could you capture them?

I bet you could not!  They are all unconditionally alive because they have 2
eyes.
