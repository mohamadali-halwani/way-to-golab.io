## アタリの石を取ってみよう

通常、19×19のボードを使用しますが、一般的にはより小さい9×9のボードを使用します。
初心者の方におすすめです。

黒盤で打って、白の石を捕獲してください。

Board::capture1

---

黒番です。白２子がアタリです。１手で取って下さい

Board::capture2

---

黒番です。白１子を取って下さい。まわりに石があっても考え方は同じです。

Board::capture3

---

上の問題を完全にわかるようになるまでくりかえして下さい。
わかるようになれば、あなたに「**囲碁50級**」を認定してあげましょう！