Le pietre nere sono completamente circondate. Nonostante ci siamo 3 spazi
interni, essi formano un solo occhio.

Trova la mossa che divida lo spazio in modo da formare due occhi.

Board::find-life-1

Se non giochi la mossa giusta, la giocherà bianco.

> Il tuo punto vitale è il punto vitale del tuo avversario.

---

Dov'è il posto migliore per dividire lo spazio formando due occhi?

Board::find-life-2

Come puoi vedere da questi due problemi, se lo spazio che circondi è piccolo,
vivere sarà difficile. Devi lottare per creare uno spazio abbastanza largo dove
poter ottenere facilmente due occhi. 

---

Considera la figura seguente.

Board::four-internal

Se bianco gioca una delle mosse segnate, nero può ancora giocare l'altra per
dividere con successo lo spazio formando due occhi.

Dato che nero può fare due occhi, possiamo considerare nero vivo anche in questa situazione.

Tuttavia, tieni a mente che se bianco riesce a giocare *entrambe* le mosse segnate, nero
può essere catturato.