### Uno schiaffo in faccia.

Qui imparerai alcune tecniche pratiche che ti serviranno quando inizierai una vera partita.

---

Sei nero, come sempre.

Bianco mette una pietra molto vicino a te - questa mossa è chiamata "mossa a contatto".
Come dovresti rispondere a questa mossa?

Board::attach0

---

In questo caso, la mossa di nero mostrata sotto è buona.
Questa mossa riduce le libertà di bianco da 3 a 2.
E' quasi come se nero stesse dando uno schiaffo in faccia a bianco.
Attacca l'aggressore!

Board::attach1

---

Continuando dalla situazione precedente, se bianco gioca altrove, per esempio E7, nero può continuare ad attaccare la pietra di bianco. Ora la pietra in E4 è in Atari - con un'altra mossa aggiuntiva, puoi catturarla. 

Board::attach2

---

Continuando dalla prima figura, se nero non risponde alla mossa a contatto di bianco e gioca altrove, per esempio F7, come giocherà bianco?

Bianco continuerà ad attaccare dando a nero uno schiaffo in faccia.
Vedi come la pietra nera in E3 è nei guai?

Board::attach3

---

Infine, è tempo di fare pratica.
Bianco ha appena giocato a contatto.
Ci sono due risposte corrette. Trovale entrambe!

Board::attach4

Non è troppo facile?