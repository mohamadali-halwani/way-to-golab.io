Ora torniamo sul goban 9x9.

Board::more-end-game-1

Siamo arrivati alla fine? Sembra che bianco abbia la vittoria
in tasca. Tuttavia...

---

Se circondi un territorio troppo ampio, puoi incorrere nel rischio
che il tuo avversario lo invada. 

Board::more-end-game-2

In questa situazione, è possibile per nero fare un gruppo vivo
all'interno del territorio bianco.

Nonostante questo possa risultare difficile a causa del muro bianco,
il territorio di bianco sarebbe drasticamente ridotto nel caso
nero riuscisse nel suo intento.

---

Board::more-end-game-3

Nonostante siamo lontani dal finegioco, bianco ha migliori prospettive.
Riesce a vedere perchè? 
