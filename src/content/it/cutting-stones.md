In questa situazione, sembra che bianco abbia il territorio maggiore.

Tuttavia, il suo muro ha un punto debole! Prova a "tagliare" il muro bianco. 

Board::cut-1

Questo mostra che è possibile tagliare le pietre che sono diagonalmente connesse.

Se le pietre sono tagliate in due parti, ogni lato dovrà vivere in maniera separata.
Questo è molto più difficile che vivere con un solo gruppo!

---

Nero ha 30 punti e bianco ha 31 punti. Sembra che sarà una vittoria di un punto
a favore di bianco.

Tuttavia, bianco ha un debolezza molto seria!

Taglia il muro bianco e guarda cosa succeda!

Board::cut-2
