Questo è chiamato un salto di due spazi, o **niken tobi**.

Board::two-point-jump

Ci sono vantaggi e svantaggi a giocare questa forma rispetto
al salto di uno.

Questa mossa garantisce più influenza coprendo un'area più grande,
ma può essere tagliata più facilmente da bianco.

A volte sono giocati anche salti da tre o quattro spazi. Quei salti
sono tipicamente giocati nell'apertura.

---

Questo salto è chiamato il **salto del cavallo**, o **keima**, dato
che ricorda il movimento del cavallo negli scacchi.

Board::keima

Questa mossa è spesso usata per fare territorio nell'angolo,
come nel diagramma.

---

Questa mossa è chiamata **mossa diagonale**, o **kosumi**.

Board::kosumi

Questa mossa perde in velocità guadagnandone in stabilità dato che
le pietre sono praticamente connesse.