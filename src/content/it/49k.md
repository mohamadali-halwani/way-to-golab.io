## Un po' più difficile

Le pietre bloccate sul bordo richiedono meno mosse all'avversario per
essere completamente circondate e sono quindi più facili da catturare.

Prova a catturare le pietre bianche che sono in **atari**.

Board::capture1

---

Una pietra nell'angolo è in **atari**.

Board::capture2

---

Ora due pietre sono in **atari**.

Board::capture3

---

Padroneggia questi concetti e avrai raggiunto il livello di **49 Kyu**!

