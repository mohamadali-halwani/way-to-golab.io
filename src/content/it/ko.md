Dopo che avrai letto questa pagina, conoscerai tutte le regole!

Il concetto di **ko** è un po' più difficile rispetto alle regole che 
abbiamo visto fino ad ora. Tanto per dare un po' di contesto, **ko** è
una parola giapponese che significa "eternità".
Vediamo come mai viene usato questo termine.

Una pietra bianca è in atari. Catturala!

Board::simple-ko

Nota che nella posizione riportata sopra, sarebbe il turno di bianco e nero
è ora in atari. Se nero venisse catturato da bianco, ritorneremo al punto
di partenza!

Si potrebbe andare avanti all'infinito! Per evitare questo, **la regola del ko
proibisce il ripetersi delle posizioni**. Puoi provare a catturare la pietra
nera nel diagramma precedente, ma non sarà permesso.

Può bianco in qualche modo ricatturare la pietra nera? 

La risposta è si, ma deve prima giocare altrove. In questo modo, si produrrà
una posizione diversa da quella iniziale.

**Puoi catturare la pietra in ko una volta che giochi prima da un'altra parte.**

Prova prima a giocare altrove e poi ricatturare la pietra in ko.

---

Quindi come si manifesta il ko nelle partite vere?

Qui puoi giocare con entrambi i colori partendo con nero. Nero può catturare
il ko dovre entrambe le parti hanno 5 pietre a rischio.

Bianco può proseguire minacciando di catturare le gruppo in basso di 8 pietre.

Board::ko-fight

Quando un giocatore gioca altrove, rispetto ad un ko, per fare una minaccia, prende
il nome di **minaccia ko**. Entrambi i giocatori posso continuare a scambiarsi
minacce ko finchè un giocatore decide che la minaccia non è abbastanza grande da
meritarsi una risposta.

Questa situazione è chiamata **battaglia ko**.

---

Le battaglie ko possono produrre situazioni incredibilmente complesse sul goban.
Per un principiante, ti basti ricordare la regola.

**Le mosse che producono la stessa posizione sono proibite.**
