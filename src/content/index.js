import {endsWith} from 'lodash';
import yaml from 'js-yaml';

export default async function content({path}) {
    let contentPath;

    try {
        // file-loader gives us a path back to a static file
        contentPath = require(`${path}`).default;
    } catch(error) {
        return null;
    }

    return new Promise((resolve, reject) => {
        fetch(contentPath).then(response => {
            response.text().then(rawResponse => {
                if (endsWith(contentPath, 'yaml')) {
                    resolve(yaml.safeLoad(rawResponse));
                } else {
                    resolve(rawResponse);
                }
            });
        });
    });
}
